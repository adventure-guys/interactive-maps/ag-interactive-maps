## 🗺️ Интерактивные карты от Adventure Guys

Также доступна на сайте [Adventure Guys](https://adventureguys.club).

### Обратная связь
Пишите об ошибках, предлагайте исправления и улучшения через специальный [Discord-сервер](https://discord.gg/xefpN9789E).
Всех активных участников добавим на сайт как соавторов карты!

### Поддержать авторов
Поддержать нас можно подпиской на:
- [Boosty](https://boosty.to/omar0275)
- [Youtube](https://www.youtube.com/channel/UCMR9Sag4E8pi2UYc3estbaw?sub_confirmation=1)
- [VK](https://vk.com/adventure_guys)
